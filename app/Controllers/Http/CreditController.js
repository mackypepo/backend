'use strict'

const Database = use('Database')
const axios = require('axios')

class CreditController {

    async FindAll(){
        const query = await Database.raw('SELECT TC.ID, TC.MEMBER_ID, M.MEMBER_USERNAME, TC.TRANSACTION_TYPE, TC.CREDIT, TC.REMARK, TC.CREATEDATE, TC.CREATE_BY FROM UFABET.TRANSACTION_CREDIT TC INNER JOIN UFABET.MEMBER M ON M.ID =  TC.MEMBER_ID ORDER BY TC.ID DESC')
        return query[0];
    }
    
    async addCreditAPI(request, response){
        var dt = new Date();
        dt.setHours(dt.getHours()+7);
        const query = Database.table('TRANSACTION_CREDIT')
        const member = Database.table('MEMBER')
        const body = request._request_._original
        let sd = 'Message Backend'
        let as = '300'
        console.log(body)	
         let getMember = await member.where('ID',body.MEMBER_ID).first()
         console.log(getMember)
            const queryWeb = Database.table('WEBSITE')
            const webAgent = await queryWeb.where('ID',getMember.WEBSITE_ID).first();
            console.log(webAgent)
            var creditt = parseFloat(body.CREDIT)
              //Deposit
              if(body.TRANSACTION_TYPE == 'deposit'){
              
                        let t = await axios({
                          method: 'post',
                            url: 'https://kraken.mrwed.cloud/partner/user/credit/add',
                            headers: { 
                            'Content-Type': 'application/json', 
                            'x-api-key': '5333dcb9-dd3b-4fd9-896a-8e42ec8968a4'
                            },
                          data : {
                            "agentUsername": webAgent.USERNAME,
                            "agentPassword": webAgent.PASSWORD,
                            "username": getMember.MEMBER_USERNAME,
                            "credit": creditt
                          }
                          })
                            console.log(t.data)   
                
                        if(t.data.status == 'success'){
                          as = '200'
                          sd = 'Add Credit in UFABET Success'
                          const addCredit = await query.insert({
                            'MEMBER_ID':getMember.ID,
                            'TRANSACTION_TYPE':body.TRANSACTION_TYPE,
                            'CREDIT':body.CREDIT,
                            'REMARK':body.REMARK,
                            'CREATEDATE':dt,
                            'CREATE_BY':body.CREATEBY,
                        })
                        if(addCredit){
                            as ='200'
                            sd = 'Add Credit Success'
                        }else{
                            as = '302'
                            sd = 'Error Server'
                        }

                        return {status :  as,
                               message :  sd};
                        }else{
                          as = '301',
                          sd = 'UFABET API ERROR'
                          return {status :  as,
                            message :  sd};
                        }
                      
                    // do something about response

                  // Withdraw
                  }else{
                   
                    let t = await axios({
                      method: 'post',
                        url: 'https://kraken.mrwed.cloud/partner/user/credit/del',
                        headers: { 
                        'Content-Type': 'application/json', 
                        'x-api-key': '5333dcb9-dd3b-4fd9-896a-8e42ec8968a4'
                        },
                      data : {
                        "agentUsername": webAgent.USERNAME,
                        "agentPassword": webAgent.PASSWORD,
                        "username": getMember.MEMBER_USERNAME,
                        "credit": creditt
                      }
                      })
                        console.log(t.data)   
            
                        if(t.data.status == 'success'){
                          as = '200'
                          sd = 'del Credit in UFABET Success'
                          const addCredit = await query.insert({
                            'MEMBER_ID':getMember.ID,
                            'TRANSACTION_TYPE':body.TRANSACTION_TYPE,
                            'CREDIT':body.CREDIT,
                            'REMARK':body.REMARK,
                            'CREATEDATE':dt,
                            'CREATE_BY':body.CREATEBY,
                        })
                        if(addCredit){
                            as ='200'
                            sd = 'Del Credit Success'
                        }else{
                            as = '302'
                            sd = 'Error Server'
                        }

                        return {status :  as,
                               message :  sd};
                        }else{
                          as = '301',
                          sd = t.data.message

                          return {status :  as,
                            message :  sd};
                        }

    }
  }

  // async addCredit(request, response){
  //     var dt = new Date();
  //     dt.setHours(dt.getHours()+7);
  //     const query = Database.table('TRANSACTION_CREDIT')
  //     const body = request._request_._original
  //     console.log(body)	

  //     try{
  //         const addCredit = await query.insert({
  //             'MEMBER_ID':body.MEMBER_ID,
  //             'TRANSACTION_TYPE':body.TRANSACTION_TYPE,
  //             'CREDIT':body.CREDIT,
  //             'REMARK':body.REMARK,
  //             'CREATEDATE':dt,
  //             'CREATE_BY':body.CREATE_BY
  //         })
  //         if(addCredit){
  //             response = 'Success'
  //         }else{
  //             response = 'Error'
  //         }            
  //         return {message : response}

  //     }catch(err){
  //         console.error(err)
  //     }
      
  // }


  async editCredit(request, response){
      const query = Database.table('TRANSACTION_CREDIT')
      const body = request._request_._original
      console.log(body)	
      var dt = new Date();
      dt.setHours(dt.getHours()+7);
      try{
          const editCredit = await query.where('ID',body.ID)
          .update({
              'MEMBER_ID':body.MEMBER_ID,
              'TRANSACTION_TYPE':body.TRANSACTION_TYPE,
              'CREDIT':body.CREDIT,
              'REMARK':body.REMARK,
              'CREATEDATE':dt,
              'CREATE_BY':body.CREATE_BY
          })
          if(editCredit){
              response = 'Success'
          }else{
              response = 'Error'
          }            
          return {message : response}

      }catch(err){
          console.error(err)
      }
      
  }

  async findByCreateDate(request , response){
    const body = request._request_._original
    let tr_fdate = body.CREATEDATE
    let tr_edate = body.ENDDATE
    let query
    let status
    let message
    console.log(body)
    if(body.USERNAME != '' && body.AGENTTYPE != ''){
      const query = await Database.raw("SELECT TC.ID AS TRANSACTION_CREDIT_ID ,TC.TRANSACTION_TYPE ,M.MEMBER_USERNAME, TC.CREDIT, TC.CREATEDATE , TC.CREATE_BY,TC.REMARK ,L.OA_NAME FROM UFABET.TRANSACTION_CREDIT TC LEFT JOIN UFABET.MEMBER M ON  TC.MEMBER_ID = M.ID LEFT JOIN UFABET.LINE_OA L ON  M.OA_ID = L.OA_ID WHERE TC.CREATEDATE BETWEEN ? AND ? AND M.MEMBER_USERNAME = ? AND TC.TRANSACTION_TYPE = ? ORDER BY TC.CREATEDATE DESC",[tr_fdate,tr_edate ,body.USERNAME , body.AGENTTYPE]);
      status = 200
      message = query[0]
    }else if(body.USERNAME !=''){
      const query = await Database.raw("SELECT TC.ID AS TRANSACTION_CREDIT_ID ,TC.TRANSACTION_TYPE ,M.MEMBER_USERNAME, TC.CREDIT, TC.CREATEDATE , TC.CREATE_BY,TC.REMARK ,L.OA_NAME FROM UFABET.TRANSACTION_CREDIT TC LEFT JOIN UFABET.MEMBER M ON  TC.MEMBER_ID = M.ID LEFT JOIN UFABET.LINE_OA L ON  M.OA_ID = L.OA_ID WHERE TC.CREATEDATE BETWEEN ? AND ? AND M.MEMBER_USERNAME = ? ORDER BY TC.CREATEDATE DESC",[tr_fdate,tr_edate ,body.USERNAME]);
      status = 200
      message = query[0]
    }else if(body.AGENTTYPE !=''){
      const query = await Database.raw("SELECT TC.ID AS TRANSACTION_CREDIT_ID ,TC.TRANSACTION_TYPE ,M.MEMBER_USERNAME, TC.CREDIT, TC.CREATEDATE , TC.CREATE_BY,TC.REMARK , L.OA_NAME FROM UFABET.TRANSACTION_CREDIT TC LEFT JOIN UFABET.MEMBER M ON  TC.MEMBER_ID = M.ID LEFT JOIN UFABET.WEBSITE W ON M.WEBSITE_ID = W.ID LEFT JOIN UFABET.LINE_OA L ON  M.OA_ID = L.OA_ID WHERE TC.CREATEDATE BETWEEN ? AND ? AND TC.TRANSACTION_TYPE = ? ORDER BY TC.CREATEDATE DESC",[tr_fdate,tr_edate ,body.AGENTTYPE]);
      status = 200
      message = query[0]
    }else{
      const query = await Database.raw("SELECT TC.ID AS TRANSACTION_CREDIT_ID ,TC.TRANSACTION_TYPE ,M.MEMBER_USERNAME, TC.CREDIT, TC.CREATEDATE , TC.CREATE_BY,TC.REMARK ,L.OA_NAME FROM UFABET.TRANSACTION_CREDIT TC LEFT JOIN UFABET.MEMBER M ON  TC.MEMBER_ID = M.ID LEFT JOIN UFABET.LINE_OA L ON  M.OA_ID = L.OA_ID WHERE TC.CREATEDATE BETWEEN ? AND ? ORDER BY TC.CREATEDATE DESC",[tr_fdate,tr_edate]);
      status = 200
      message = query[0]
    }
    return {status : status,
      message : message}
  }

  async sumCredit(request , response){
    const body = request._request_._original
    let tr_fdate = body.CREATEDATE
    let tr_edate = body.ENDDATE
    let query
    let result
    let status
    let message1
    let message2
      query = await Database.raw("SELECT IFNULL((SELECT SUM(TC.CREDIT) FROM UFABET.TRANSACTION_CREDIT TC WHERE  TC.CREATEDATE BETWEEN ? AND ? AND TC.TRANSACTION_TYPE = 'deposit'),0) AS DEPOSIT, IFNULL((SELECT SUM(TC.CREDIT) FROM UFABET.TRANSACTION_CREDIT TC WHERE  TC.CREATEDATE BETWEEN ? AND ? AND TC.TRANSACTION_TYPE = 'withdraw'),0) AS WITHDRAW, IFNULL(((SELECT SUM(TC.CREDIT) FROM UFABET.TRANSACTION_CREDIT TC WHERE  TC.CREATEDATE BETWEEN ? AND ? AND TC.TRANSACTION_TYPE = 'deposit') - (SELECT SUM(TC.CREDIT) FROM UFABET.TRANSACTION_CREDIT TC WHERE  TC.CREATEDATE BETWEEN ? AND ? AND TC.TRANSACTION_TYPE = 'withdraw')),0) AS NET_PROFIT , IFNULL((SELECT SUM(TC.CREDIT) FROM UFABET.TRANSACTION_CREDIT TC WHERE  TC.CREATEDATE BETWEEN ? AND ? AND TC.TRANSACTION_TYPE = 'return'),0) AS RE_TURN FROM DUAL",[tr_fdate,tr_edate ,tr_fdate,tr_edate,tr_fdate,tr_edate,tr_fdate,tr_edate,tr_fdate,tr_edate]);
      result = await Database.raw("SELECT IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? ),0) AS SUM_TRANSACTION_DEPOSIT, IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? ),0) AS SUM_TRANSACTION_WITHDAW, IFNULL(((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN ? AND ? ) - (SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? )),0) AS NET_PROFIT FROM DUAL",[tr_fdate,tr_edate ,tr_fdate,tr_edate,tr_fdate,tr_edate,tr_fdate,tr_edate]);
      status = 200
      message1 = query[0][0]
      message2 = result[0][0]

      return {status : status,
             credit : message1,
             trx:message2}
    }


    async returnCredit(request , response){
      var dt = new Date();
      //dt.setHours(dt.getHours()+7);
      var countS = 0;
      var ss = 0;
      var summmnet= 0;
      const query = Database.table('TRANSACTION_CREDIT')
      var fd = '2021-08-30'
      var ld = '2021-09-06'
          var member = await Database.raw("SELECT M.ID, M.MEMBER_USERNAME ,(SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID) AS SUM_COUNT_TD, IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TD, (SELECT COUNT(*) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID) AS SUM_COUNT_TW, IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TW, (IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) - IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? AND TW.MEMBER_ID = M.ID),0)) AS SUM_NET FROM UFABET.MEMBER M LEFT JOIN WEBSITE W ON M.WEBSITE_ID = W.ID WHERE  ((SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.MEMBER_ID = M.ID AND TD.UPDATEDATE BETWEEN ? AND ?) > 0) UNION SELECT M.ID, M.MEMBER_USERNAME ,(SELECT COUNT(*) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID) AS SUM_COUNT_TD, IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TD, (SELECT COUNT(*) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID) AS SUM_COUNT_TW, IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN  ? AND ? AND TW.MEMBER_ID = M.ID),0) AS SUM_BALANCE_TW, (IFNULL((SELECT SUM(TD.BALANCE) FROM UFABET.TRANSACTION_DEPOSIT TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.UPDATEDATE BETWEEN  ? AND ? AND TD.MEMBER_ID = M.ID),0) - IFNULL((SELECT SUM(TW.BALANCE) FROM UFABET.TRANSACTION_WITHDAW TW WHERE TW.TRANSACTION_STATUS = 'success' AND TW.UPDATEDATE BETWEEN ? AND ? AND TW.MEMBER_ID = M.ID),0)) AS SUM_NET FROM UFABET.MEMBER M LEFT JOIN WEBSITE W ON M.WEBSITE_ID = W.ID WHERE  ((SELECT COUNT(*) FROM UFABET.TRANSACTION_WITHDAW TD WHERE TD.TRANSACTION_STATUS = 'success' AND TD.MEMBER_ID = M.ID AND TD.UPDATEDATE BETWEEN ? AND ?) > 0) order by ID" ,[fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld,fd,ld, fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld ,fd , ld,fd,ld,])
          var rsult = member[0].length
          member = member[0]
        //  console.log(member)
          for(var index =0 ; index < rsult ; index++){
            //console.log(member[index].SUM_NET)
            //if(member[index].SUM_NET >= 300 &&  member[index].SUM_NET <2000){

             if(member[index].SUM_NET >= 300){
            //Select SQL for Check Data Return 
              console.log(member[index].MEMBER_USERNAME)
             
              var fv = '2021-09-05'
              var lv = '2021-09-08'
              var checkdata = await Database.raw("SELECT * FROM UFABET.TRANSACTION_CREDIT TC WHERE TC.MEMBER_ID = ? AND TC.TRANSACTION_TYPE = 'return' AND CREATEDATE BETWEEN  ? AND ?",[member[index].ID ,fv,lv ])
            //   // IF check data in TRX Credit≈
             // console.log(checkdata[0])
              if(checkdata[0] == ''){
                ss++
                console.log('ยังไม่ได้ทำ')
                var returnValue = parseInt(member[index].SUM_NET * 0.05) 
                if(returnValue >= 2000){
                returnValue = 2000
                }
                console.log('ยอดเสีย = ' + member[index].SUM_NET + 'ยอดคืน = '+returnValue)
                countS= countS+returnValue
                
              // Call Api UFA for Return Credit 
             // console.log(member[index].MEMBER_USERNAME  + returnValue)

              let t = await axios({
                  method: 'post',
                  url: 'https://kraken.mrwed.cloud/partner/user/credit/add',
                  headers: { 
                  'Content-Type': 'application/json', 
                  'x-api-key': '5333dcb9-dd3b-4fd9-896a-8e42ec8968a4'
                  },
                data : {
                  "agentUsername": "",
                  "agentPassword": "",
                  "username": member[index].MEMBER_USERNAME,
                  "credit": returnValue
                }
                })
                  
      
              if(t.data.status == 'success'){
                
                const addCredit = await query.insert({
                  'MEMBER_ID':member[index].ID,
                  'TRANSACTION_TYPE': 'return',
                  'CREDIT':returnValue,
                  'REMARK':'คืนยอดเสีย',
                  'CREATEDATE':dt,
                  'CREATE_BY':'System',
              })
              if(addCredit){
                  
                  console.log('คืนยอดเสีย  '+ member[index].MEMBER_USERNAME +' สำเร็จ')
              }else{
                
                  console.log('insert DB Error' + member[index].MEMBER_USERNAME + '  ' +returnValue)
              }
              
              }else{
            
                console.log('API ERROR')
              }

              }else{
                console.log('ทำแล้ว'+member[index].ID)

                
              }

           
            
            }else{
              // console.log(member[index].MEMBER_USERNAME +'น้อยกว่านะไอสัส')

            }


          }
          
          var status = 200 
          console.log(rsult)
          console.log(countS)
          console.log(ss)
  

      return {   status : '200' ,
                messsage : 'mackylovecake',
                message2 : member}

    }

}

module.exports = CreditController