'use strict'
const Database = use('Database')

class WebsiteController {

    async FindAll(){
        const query = Database.table('WEBSITE')
        const website = await query
        return website;
    }

}


module.exports = WebsiteController
